package ir.os.patients.model

import org.junit.Assert.*
import org.junit.Test

class ToolsTest {

    @Test
    fun getTimeStamp() {
        val s = "01,01,1970"
        println(Tools.getTimeStamp(s))
        assertTrue(Tools.getTimeStamp(s) > (System.currentTimeMillis() / 1000) - 630000000)
    }
}