package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.ListClickListener;
import ir.os.patients.model.Task;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;

public class History extends Activity implements View.OnClickListener {

    RecyclerView list;
    UserStore store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setSupportActionBar(findViewById(R.id.toolbar));
        init();
    }

    @SuppressLint("CheckResult")
    private void init() {
        store = new UserStore(this);
        setTitle(store.getPatient().getName() + "'s Illnesses");
        list = findViewById(R.id.list);
        (findViewById(R.id.fab)).setOnClickListener(this);
        list.setHasFixedSize(true);
        list.setLayoutManager(new GridLayoutManager(this, 2));
        AppDatabase.getInstance(this)
                .historyDao()
                .getPatientIllnesses(store.getPatient().getNationalCode())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(strings -> {
                    if (strings.size() == 0) toast("You have added no illnesses.");
                    else list.setAdapter(new MyAdapter(this, strings));
                }, throwable -> {
                    Log.e("Error#History", throwable.getMessage());
                    toast("Problem interacting with database.");
                });
        listClicked();
    }

    private void listClicked() {
        list.addOnItemTouchListener(new ListClickListener(this, list, (view, position) -> {
            List<String> ills = ((MyAdapter) list.getAdapter()).set;
            String ill = ((MyAdapter) list.getAdapter()).set.get(position);
            new AlertDialog.Builder(this)
                    .setTitle("Delete")
                    .setMessage("Delete this illness from your history?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        new Task(() -> {
                            AppDatabase.getInstance(this)
                                    .historyDao().delete(new ir.os.patients.model.database.History(
                                    store.getPatient().getNationalCode(),
                                    ill));
                        }, (r) -> {
                            if (r) {
                                toast("Done!");
                                ills.remove(position);
                                list.getAdapter().notifyItemRemoved(position);
                            } else {
                                toast("Problem deleting illness. Try again.");
                            }
                        }).execute();
                    })
                    .setNegativeButton("No", null)
                    .create().show();
        }));
    }

    @Override
    public void onClick(View v) {
        //Fab
        EditText e = new EditText(this);
        e.setInputType(InputType.TYPE_CLASS_TEXT);
        new AlertDialog.Builder(this)
                .setTitle("Add").setMessage("Enter name of illness")
                .setView(e)
                .setPositiveButton("Add", (dialog, which) -> {
                    if (e.getText().toString().isEmpty()) toast("Please enter name of illness.");
                    else {
                        new Task(() -> {
                            AppDatabase.getInstance(this)
                                    .historyDao()
                                    .insert(new ir.os.patients.model.database.History(store.getPatient().getNationalCode(),
                                            e.getText().toString()));
                        }, (r) -> {
                            if (r) {
                                toast("Done!");
                                recreate();
                                overridePendingTransition(0, 0);
                            } else {
                                toast("Failed to add illness. Try again.");
                            }
                        }).execute();
                    }
                })
                .setNegativeButton("Cancel", null)
                .create().show();
    }

    //List
    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<String> set;

        public MyAdapter(Context c, List<String> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(c).inflate(R.layout.li_6, parent, false));
        }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.illness.setText(set.get(position));
        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView illness;

        public ViewHolder(View itemView) {
            super(itemView);
            illness = itemView.findViewById(R.id.illness);
        }
    }
}
