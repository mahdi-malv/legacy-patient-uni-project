package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.model.Activity;
import ir.os.patients.model.ListClickListener;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Doctor;
import java.util.List;

import ir.os.patients.R;

public class SeeDoctors extends Activity {

    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_doctors);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle("Doctors");
        init();
    }

    @SuppressLint("CheckResult")
    private void init() {
        list = findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(true);
        AppDatabase.getInstance(this)
                .doctorDao()
                .getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(doctors -> {
                    if (doctors.size() == 0) toast("No doctors registered!");
                    else list.setAdapter(new MyAdapter(this, doctors));
                }, throwable -> {
                    toast("Problem interacting with database");
                });
        listClicked();
    }

    private void listClicked() {
        list.addOnItemTouchListener(new ListClickListener(this, list, (view, position) -> {
            Doctor d = ((MyAdapter) list.getAdapter()).set.get(position);
            startActivity(new Intent(this, DoctorTimes.class).putExtra("nc", d.getNationalCode()));
        }));
    }


    //List
    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<Doctor> set;

        public MyAdapter(Context c, List<Doctor> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(c).inflate(R.layout.li_2, parent, false));
        }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Doctor a = set.get(position);
            holder.name.setText("Dr. " + a.getName());
            holder.specially.setText("Specially: " + a.getSpecially());
        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, specially;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            specially = itemView.findViewById(R.id.specially);
        }
    }
}
