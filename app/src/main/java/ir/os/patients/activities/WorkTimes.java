package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.DoctorWorkTime;
import ir.os.patients.model.SetDate;
import ir.os.patients.model.Task;
import ir.os.patients.model.Tools;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Timing;

public class WorkTimes extends Activity {

    String nc;
    UserStore store;
    RecyclerView list;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_times);
        setSupportActionBar(findViewById(R.id.toolbar));
        init();
        initList();
    }

    @SuppressLint("CheckResult")
    private void initList() {
        AppDatabase.getInstance(this).timingDao().getByNc(store.getDoctor().getNationalCode())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(timings -> {
                    System.out.println(timings.toString());
                    list.setAdapter(new MyAdapter(this, timings));
                }, throwable -> {
                    toast("Doctor got no work time.");
                });
    }

    private void init() {
        store = new UserStore(this);
        setTitle("Dr. " + store.getDoctor().getName() + "'s work times.");
        fab = findViewById(R.id.fab);
        list = findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(true);
        fab.setOnClickListener(v -> {
            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.VERTICAL);
            EditText id = new EditText(this), date = new EditText(this), hour = new EditText(this);
            id.setInputType(InputType.TYPE_CLASS_NUMBER);
            id.setHint("Building id");
            new SetDate(this, date);
            date.setFocusable(false);
            date.setHint("Date");
            hour.setInputType(InputType.TYPE_CLASS_NUMBER);
            hour.setHint("Hour(0 to 23)");
            l.addView(id);
            l.addView(date);
            l.addView(hour);
            new AlertDialog.Builder(this)
                    .setTitle("Add worktime").setMessage("Please enter information")
                    .setView(l)
                    .setPositiveButton("Save", (dialog, which) -> {
                        AppDatabase.getInstance(this)
                                .verifyDao()
                                .getByBuildingId(Tools.Companion.getNumber(id))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.computation())
                                .subscribe(verify -> {
                                    //Verified
                                    if (Tools.Companion.getNumber(hour) >= 0 && Tools.Companion.getNumber(hour) <= 23) {
                                        new Task(() ->
                                                AppDatabase.getInstance(this).timingDao()
                                                        .insert(new Timing(store.getDoctor().getNationalCode(), Tools.Companion.getNumber(id),
                                                                date.getText().toString(), Tools.Companion.getNumber(hour), false))
                                                ,
                                                (r) -> {
                                                    if (r) {
                                                        toast("Successfully added to your schedule.");
                                                        recreate();
                                                        overridePendingTransition(0, 0);
                                                    } else {
                                                        toast("Problem occurred. Please try again.");
                                                    }
                                                }).execute();
                                    }
                                }, throwable -> {
                                    //Not verified
                                    toast("Not verified or existing Building.");
                                });
                    })
                    .setNegativeButton("Cancel", null)
                    .create().show();

        });
    }

    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<Timing> set;

        public MyAdapter(Context c, List<Timing> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(
                    LayoutInflater.from(c).inflate(R.layout.li_4, parent, false)
            );
        }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Timing d = set.get(position);
            holder.date.setText(d.getDate() + " , " + d.getHour() + ":00");
            holder.buildingId.setText("Building id: " + d.getBuildingId());
            holder.taken.setText(d.isTaken() ? "Reserved." : "Free");
            holder.taken.setTextColor(getResources().getColor(d.isTaken()? R.color.red : R.color.green));

        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date, buildingId, taken;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            buildingId = itemView.findViewById(R.id.building_id);
            taken = itemView.findViewById(R.id.taken);
        }
    }
}
