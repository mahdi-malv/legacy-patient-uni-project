package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.Appointments;
import ir.os.patients.model.ListClickListener;
import ir.os.patients.model.Task;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Appointment;

public class Patient extends Activity implements View.OnClickListener {

    RecyclerView list;
    UserStore store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);
        setSupportActionBar(findViewById(R.id.toolbar));
        init();
    }

    @SuppressLint("CheckResult")
    private void init() {
        store = new UserStore(this);
        list = findViewById(R.id.list);
        findViewById(R.id.fab).setOnClickListener(this);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(this));
        AppDatabase.getInstance(this)
                .appointmentDao()
                .getAppoinmentsForPatient(store.getPatient().getNationalCode())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(appointments -> {
                    if (appointments.size() != 0)
                        list.setAdapter(new MyAdapter(this, appointments));
                    else toast("You have no appointments");
                }, throwable -> toast("Problem interacting with database"));

        list.addOnItemTouchListener(new ListClickListener(this, list, (view, position) -> {
            List<Appointments> appointments = ((MyAdapter)list.getAdapter()).set;
            new AlertDialog.Builder(this)
                    .setTitle("Complete")
                    .setMessage("Press Ok to cancel")
                    .setPositiveButton("Ok", (dialogInterface, i) -> {
                        toast("Appointment can't be canceled now, Some APIs need changes.");
                        new Task(()->{
//                            AppDatabase.getInstance(this).appointmentDao().delete();
                            // TODO: Find a way to delete this uniquely.
                        },(r)->{});
                    })
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show();
        }));
    }

    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Edit history");
        menu.add(0, 1, 1, "Logout");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(this, History.class));
                break;
            case 1:
                new UserStore(this).logout();
                startActivity(new Intent(this, Main.class));
                finishAffinity();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        //Fab
        //Go see doctors
        startActivity(new Intent(this, SeeDoctors.class));
    }

    //List
    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<Appointments> set;

        public MyAdapter(Context c, List<Appointments> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(c).inflate(R.layout.li_1, parent, false));
        }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Appointments a = set.get(position);
            holder.doctorName.setText("Dr. " + a.getName());
            holder.date.setText("Date: " + a.getDate() + " - " + a.getHour() + ":00");
            holder.completed.setText(a.isCompleted() ? "Done!" : "Not-Done!");
            holder.completed.setTextColor(getResources().getColor(a.isCompleted() ? R.color.green : R.color.red));
        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView doctorName, date, completed;

        public ViewHolder(View itemView) {
            super(itemView);
            doctorName = itemView.findViewById(R.id.doctorName);
            date = itemView.findViewById(R.id.date);
            completed = itemView.findViewById(R.id.completed);
        }
    }
}
