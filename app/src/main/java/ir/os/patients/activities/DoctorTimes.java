package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.ListClickListener;
import ir.os.patients.model.Task;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Appointment;

public class DoctorTimes extends Activity {

    RecyclerView list;
    String nc = "";
    UserStore store;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_times);
        setSupportActionBar(findViewById(R.id.toolbar));
        nc = getIntent().getStringExtra("nc");
        setTitle("Doctor");
        //Get Doctor name
        AppDatabase.getInstance(this).doctorDao().getByNc(nc).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(doctor -> setTitle("Dr. " + doctor.getName()));
        init();
    }

    @SuppressLint("CheckResult")
    private void init() {
        store = new UserStore(this);
        list = findViewById(R.id.list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(this));
        AppDatabase.getInstance(this)
                .timingDao()
                .getDoctorTimes(nc)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(doctorTimes -> {
                    if (doctorTimes.size() == 0) {
                        toast("No times found! Try again later.");
                    } else list.setAdapter(new MyAdapter(this, doctorTimes));
                }, throwable -> {
                    toast("Problem interacting with database.");
                });
        listClicked();
    }

    private void listClicked() {
        list.addOnItemTouchListener(new ListClickListener(this, list, (view, position) -> {
            List<ir.os.patients.model.DoctorTimes> ts = ((MyAdapter) list.getAdapter()).set;
            ir.os.patients.model.DoctorTimes t = ts.get(position);
            if (t.isTaken()) {
                toast("Can't do anything with that.");
            } else {
                //Add to appointment and update timing
                new Task(()->{
                    AppDatabase.getInstance(this).appointmentDao().insert(
                            new Appointment(store.getPatient().getNationalCode(),
                                    nc, t.getDate(), t.getHour(), false));
                    AppDatabase.getInstance(this)
                            .timingDao().updateTaken(t.getId(), nc, true, t.getDate(), t.getHour());
                }, (r)->{
                    if (r) {
                        toast("Appointment set. Please be there on time.");
                    } else toast("Problem occurred. Try again.");
                }).execute();
            }
        }));
    }

    //List
    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<ir.os.patients.model.DoctorTimes> set;

        public MyAdapter(Context c, List<ir.os.patients.model.DoctorTimes> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(c).inflate(R.layout.li_7, parent, false));
        }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            ir.os.patients.model.DoctorTimes t = set.get(position);
            holder.date.setText(t.getDate() + " - " + t.getHour() + ":00");
            holder.address.setText(t.getAddress());
            holder.phone.setText(t.getPhone());
            holder.taken.setText(t.isTaken() ? "TAKEN!": "Free");
            holder.taken.setTextColor(getResources().getColor(t.isTaken() ? R.color.red : R.color.green));
        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date, phone, address, taken;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            phone = itemView.findViewById(R.id.phone);
            address = itemView.findViewById(R.id.address);
            taken = itemView.findViewById(R.id.taken);
        }
    }
}
