package ir.os.patients.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.SetDate;
import ir.os.patients.model.Task;
import ir.os.patients.model.Tools;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.License;
import ir.os.patients.model.database.Patient;

public class RegisterPatient extends Activity {

    EditText nameEdit, ncEdit, passEdit, phoneEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_patient);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle("Register");
        init();
    }

    private void init() {
        state = PATIENT;
        nameEdit = findViewById(R.id.nameEdit);
        ncEdit = findViewById(R.id.ncEdit);
        phoneEdit  = findViewById(R.id.phoneEdit);
        passEdit = findViewById(R.id.passEdit);
        findViewById(R.id.register).setOnClickListener(this::signUp);
    }


    private void signUp(View v) {
        //TODO Validate specially and address
        System.out.println("Name: " + Tools.Companion.validateName(nameEdit.getText().toString()));
        System.out.println("Pass: " + Tools.Companion.validatePassword(nameEdit.getText().toString()));
        System.out.println("NC" + Tools.Companion.validateNationalCode(ncEdit));
        System.out.println("Phone" + Tools.Companion.validatePhoneNumber(phoneEdit.getText().toString()));
        boolean isValid = Tools.Companion.validateName(nameEdit.getText().toString()) &&
                Tools.Companion.validatePassword(passEdit.getText().toString()) &&
                Tools.Companion.validateNationalCode(ncEdit) &&
                Tools.Companion.validatePhoneNumber(phoneEdit.getText().toString());
        if (isValid) {
            new Task(() -> {
                AppDatabase.getInstance(this).patientDao()
                        .insert(new Patient(
                                ncEdit.getText().toString(), phoneEdit.getText().toString(),
                                nameEdit.getText().toString(), passEdit.getText().toString()
                        ));
            }, this::handleResult).execute();
        } else toast("Invalid fields.");
    }

    private void handleResult(boolean r) {
        if (r) {
            toast("Welcome dear user!");
            new UserStore(this)
                    .saveUser(new Patient(
                            ncEdit.getText().toString(),
                            phoneEdit.getText().toString(),
                            nameEdit.getText().toString(),
                            passEdit.getText().toString()
                    ));
            finishAffinity();
            startActivity(new Intent(this, ir.os.patients.activities.Patient.class));
        } else {
            toast("Insert to database failed. National code might be taken.");
        }
    }
}
