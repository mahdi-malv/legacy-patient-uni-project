package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.opengl.ETC1;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.ListClickListener;
import ir.os.patients.model.Task;
import ir.os.patients.model.Tools;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Building;
import ir.os.patients.model.database.Verify;
import ir.os.patients.model.database.WorkIn;

@SuppressWarnings("ALL")
public class Doctor extends Activity implements View.OnClickListener {

    TextView specially;
    RecyclerView list;
    UserStore store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);
        setSupportActionBar(findViewById(R.id.toolbar));
        init();
        setTitle("Dr. " + store.getDoctor().getName());
        setSpecially();
        initWorkPlaces();
        /*
        Some things must be initialized:
            1. The specially
            2. The list
            3. The add button
            4. Menu with LOGout option
            5. see work times button
         */
    }

   @SuppressLint("CheckResult")
    private void initWorkPlaces() {
        store = new UserStore(this);
        AppDatabase.getInstance(this)
                .buildingDao()
                .getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(buildings -> list.setAdapter(new MyAdapter(this, buildings)), throwable -> {
                    Log.e("Error#Admin", throwable.getMessage());
                    toast("داده ای از دیتابیس دریافت نشد.");
                });
    }

    private void setSpecially() {
        specially.setText(store.getDoctor().getSpecially());
        specially.setOnClickListener(v -> {
            EditText e = new EditText(this);
            e.setInputType(InputType.TYPE_CLASS_TEXT);
            e.setText(store.getDoctor().getSpecially());
            new AlertDialog.Builder(this).setTitle("Edit")
                    .setMessage("Enter new Specialty:")
                    .setView(e)
                    .setPositiveButton("Save", (dialog, which) -> {
                        if (e.getText().toString().isEmpty()) {
                            toast("No skills? Can't happen Doc.");
                        } else {
                            new Task(()-> AppDatabase.getInstance(this).doctorDao()
                                    .updateSpecially(e.getText().toString(), store.getDoctor().getNationalCode())
                                    , (r)->{
                                if (r) {
                                    toast("Done!");
                                    specially.setText(e.getText().toString());
                                } else {
                                    toast("Something happened!");
                                }
                            }).execute();
                            store.updateSpecially(e.getText().toString());
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .create().show();
        });
    }


    private void init() {
        store = new UserStore(this);
        specially = findViewById(R.id.specially);
        list = findViewById(R.id.list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(this));
        findViewById(R.id.seeWorkTimes).setOnClickListener(this);
        findViewById(R.id.addWorkPlace).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.seeWorkTimes:
                startActivity(new Intent(this, WorkTimes.class));
                break;
            case R.id.addWorkPlace:
                addWorkPlace();
                break;
        }
    }

    private void addWorkPlace() {
        LinearLayout l = new LinearLayout(this);
        l.setOrientation(LinearLayout.VERTICAL);
        EditText phone = new EditText(this), address = new EditText(this);
        phone.setInputType(InputType.TYPE_CLASS_NUMBER);
        phone.setHint("Phone: 0...");
        address.setInputType(InputType.TYPE_CLASS_TEXT);
        address.setHint("Address");
        phone.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        address.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        l.addView(phone);
        l.addView(address);
        verifyAndAddWorkPlace(l, phone, address);
    }

    private void verifyAndAddWorkPlace(LinearLayout l, EditText phone, EditText address) {
        new AlertDialog.Builder(this).setTitle("Add")
                .setMessage("Enter Workplace Info")
                .setView(l)
                .setPositiveButton("Add", (dialog, which) -> {
                    boolean isValid = Tools.Companion.validateWorkPhone(phone.getText().toString()) &&
                            !address.getText().toString().isEmpty();
                    if (isValid) {
                        new Task(()->{
                            AppDatabase.getInstance(this)
                                    .buildingDao()
                                    .insert(new Building(address.getText().toString(), phone.getText().toString(), store.getDoctor().getNationalCode()));
                        }, (r)->{
                            if (r) {
                                toast("Successful. Now wait for admin to verify it.");
                            } else {
                                toast("Unable to insert. Something happened.");
                            }
                        }).execute();
                    }
                })
                .setNegativeButton("Cancel", null)
                .create().show();
    }

    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Logout");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                new UserStore(this).logout();
                startActivity(new Intent(this, Main.class));
                finishAffinity();
                break;
            default:
                break;
        }
        return true;
    }

    //List
    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<Building> set;

        public MyAdapter(Context c, List<Building> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(
                    LayoutInflater.from(c).inflate(R.layout.li_5, parent, false)
            );
        }

        @SuppressLint({"CheckResult", "SetTextI18n"})
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.id.setText("Building id: " + set.get(position).getId());
            holder.phone.setText("Building phone: " + set.get(position).getPhone());
            holder.address.setText("Adr: " + set.get(position).getAddress());
            AppDatabase.getInstance(c)
                    .verifyDao()
                    .getByBuildingId(set.get(position).getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe(verify -> {
                        holder.verified.setText("VERIFIED");
                        holder.verified.setTextColor(getResources().getColor(R.color.green));
                    }, throwable -> {
                        holder.verified.setText("Not-Verified");
                        holder.verified.setTextColor(getResources().getColor(R.color.red));
                    });
        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView id, phone, address, verified;

        public ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            phone = itemView.findViewById(R.id.phone);
            address = itemView.findViewById(R.id.address);
            verified = itemView.findViewById(R.id.verified);
        }
    }

    //region BackPressing
    private static long backPressed;
    @Override
    public void onBackPressed(){
        if (backPressed + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
            finishAffinity();
        }
        else {
            toast("Press back again to Exit");
            backPressed = System.currentTimeMillis();
        }
    }
    //endregion
}
