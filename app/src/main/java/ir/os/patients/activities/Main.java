package ir.os.patients.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.Task;
import ir.os.patients.model.Tools;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;

public class Main extends Activity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    EditText ncEdit, passEdit;
    RadioGroup group;
    RadioButton doctor, patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle(getString(R.string.app_name) + " " + getString(R.string.version));
        init();
        doctor.setChecked(true);
        UserStore s = new UserStore(this);
        if (s.getUserLogged()) {
            switch (s.getUserType()) {
                case PATIENT:
                    state = PATIENT;
                    startActivity(new Intent(this, Patient.class));
                    break;
                case DOCTOR:
                    state = DOCTOR;
                    startActivity(new Intent(this, Doctor.class));
                    break;
                case ADMIN:
                    state = ADMIN;
                    startActivity(new Intent(this, Admin.class));
                    break;
            }
        }
    }

    private void init() {
        ncEdit = findViewById(R.id.ncEdit);
        passEdit = findViewById(R.id.passEdit);
        doctor = findViewById(R.id.doctor);
        patient = findViewById(R.id.patient);
        group = findViewById(R.id.group);
        group.setOnCheckedChangeListener(this);
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
        //Hold login button

        findViewById(R.id.login).setOnLongClickListener(v -> {
            new Task(() -> {
                AppDatabase.getInstance(this)
                        .adminDao()
                        .getAdmin(Tools.Companion.getNumber(ncEdit))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.computation())
                        .subscribe(admin -> {
                            if (admin.getPass().equals(passEdit.getText().toString())) {
                                state = ADMIN;
                                toast("You are an Admin!");
                                startActivity(new Intent(this, Admin.class));
                                new UserStore(this).saveUser(admin);
                                finishAffinity();
                            } else {
                                toast("Wrong admin password");
                            }
                        }, throwable -> toast("Admin invalid."));
            }, r -> {
            }).execute();
            return true;
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login) {
            String nc = ncEdit.getText().toString(),
                    pass = passEdit.getText().toString();
            boolean isValid = !nc.isEmpty()
                    && !TextUtils.isEmpty(pass);
            if (isValid) {

                AppDatabase d = AppDatabase.getInstance(this);
                switch (state) {
                    case PATIENT:
                        d.patientDao()
                                .getByNc(nc)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.computation())
                                .subscribe(patient -> {
                                    if (patient.getPass().equals(pass)) {
                                        new UserStore(this).saveUser(patient);
                                        startActivity(new Intent(this, Patient.class));
                                        finishAffinity();
                                    } else {
                                        Toast.makeText(this, "Wrong password.", Toast.LENGTH_SHORT).show();
                                    }
                                }, throwable -> Toast.makeText(this, "Account Not registered.",
                                        Toast.LENGTH_SHORT).show());
                        break;

                    case DOCTOR:
                        d.doctorDao()
                                .getByNc(nc)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.computation())
                                .subscribe(doctor -> {
                                    if (doctor.getPass().equals(pass)) {
                                        new UserStore(this).saveUser(doctor);
                                        startActivity(new Intent(this, Doctor.class));
                                        finishAffinity();
                                    } else {
                                        Toast.makeText(this, "Wrong Password.", Toast.LENGTH_SHORT).show();
                                    }
                                }, throwable -> {
                                    Toast.makeText(this, "User not registered.", Toast.LENGTH_SHORT).show();
                                });
                }
            } else {
                Toast.makeText(this, "Fill all fields.", Toast.LENGTH_SHORT).show();
            }//is No valid
        } else if (v.getId() == R.id.register) {
            //Register due State
            if (state == DOCTOR) startActivity(new Intent(this, RegisterDoctor.class));
            else if (state == PATIENT) startActivity(new Intent(this, RegisterPatient.class));
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == doctor.getId()) {
            state = DOCTOR;
        } else {
            state = PATIENT;
        }
    }
}
