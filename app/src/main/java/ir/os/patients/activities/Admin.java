package ir.os.patients.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.ListClickListener;
import ir.os.patients.model.Task;
import ir.os.patients.model.Tools;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Building;
import ir.os.patients.model.database.Verify;

public class Admin extends Activity {

    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle("Admin: " + new UserStore(this).getAdmin().getName());
        state = ADMIN;
        list = findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(false);
        initList();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("CheckResult")
    private void initList() {
        AppDatabase.getInstance(this)
                .buildingDao()
                .getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(buildings -> {
                    list.setAdapter(new MyAdapter(this, buildings));
                }, throwable -> {
                    Log.e("Error#Admin", throwable.getMessage());
                    toast("داده ای از دیتابیس دریافت نشد.");
                });
        list.addOnItemTouchListener(new ListClickListener(this, list, (view, position) -> {
            List<Building> set = ((MyAdapter) list.getAdapter()).set;
            new AlertDialog.Builder(this)
                    .setTitle("Verify?")
                    .setMessage("Do you want to verify the Property?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        new Task(() -> {
                            AppDatabase.getInstance(this)
                                    .verifyDao()
                                    .insert(new Verify(new UserStore(this).getAdmin().getId(),
                                            set.get(position).getId(),
                                            System.currentTimeMillis()));
                        }, (r) -> {
                            if (r) {
                                toast("Done!");
                                list.getAdapter().notifyItemChanged(position);
                            } else {
                                toast("Not possible. Is it verified before?");
                            }
                        }).execute();
                    })
                    .setNegativeButton("No", null)
                    .create().show();
        }));
    }


    class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        Context c;
        List<Building> set;

        public MyAdapter(Context c, List<Building> set) {
            this.c = c;
            this.set = set;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(
                    LayoutInflater.from(c).inflate(R.layout.li_5, parent, false)
            );
        }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.id.setText(String.valueOf(set.get(position).getId()));
            holder.phone.setText(set.get(position).getPhone());
            holder.address.setText(set.get(position).getAddress());
            AppDatabase.getInstance(c)
                    .verifyDao()
                    .getByBuildingId(set.get(position).getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe(verify -> {
                        holder.verified.setText("VERIFIED by admin id: " + verify.getAdminId());
                        holder.verified.setTextColor(getResources().getColor(R.color.green));
                    }, throwable -> {
                        holder.verified.setText("Not-Verified");
                        holder.verified.setTextColor(getResources().getColor(R.color.red));
                    });
        }

        @Override
        public int getItemCount() {
            return set.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView id, phone, address, verified;

        public ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            phone = itemView.findViewById(R.id.phone);
            address = itemView.findViewById(R.id.address);
            verified = itemView.findViewById(R.id.verified);
        }
    }

    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Edit Password");
        menu.add(0, 1, 1, "Logout");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                EditText pass = new EditText(this);
                pass.setInputType(InputType.TYPE_CLASS_NUMBER);
                pass.setHint("Password");
                pass.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                ));
                ir.os.patients.model.database.Admin a = new UserStore(this).getAdmin();
                pass.setText(a.getPass());

                new AlertDialog.Builder(this)
                        .setTitle("Edit pass")
                        .setMessage("Edit your new password and click save.")
                        .setView(pass)
                        .setPositiveButton("Save", (dialog, which) -> {
                            if (pass.getText().toString().length() < 4) {
                                toast("Not valid values. Try again.");
                            } else {
                                new Task(() -> {
                                    AppDatabase.getInstance(this)
                                            .adminDao()
                                            .update(a.getId(), pass.getText().toString());
                                }, (r) -> {
                                    if (r) {
                                        toast("Done!");
                                        new UserStore(this).updatePass(pass.getText().toString());
                                        recreate();
                                        overridePendingTransition(0, 0);
                                    } else {
                                        toast("Problem occurred. Maybe id is taken.");
                                    }
                                }).execute();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create().show();
                break;
            case 1:
                new UserStore(this).logout();
                startActivity(new Intent(this, Main.class));
                finishAffinity();
                break;
            default:
                break;
        }
        return true;
    }

    private static long backPressed;

    @Override
    public void onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            finishAffinity();
        } else {
            toast("Press back again to Exit.");
            backPressed = System.currentTimeMillis();
        }
    }
}
