package ir.os.patients.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.os.patients.R;
import ir.os.patients.model.Activity;
import ir.os.patients.model.SetDate;
import ir.os.patients.model.Task;
import ir.os.patients.model.Tools;
import ir.os.patients.model.UserStore;
import ir.os.patients.model.database.AppDatabase;
import ir.os.patients.model.database.Doctor;

public class RegisterDoctor extends Activity {

    EditText nameEdit, ncEdit, passEdit, birthdateEdit, speciallyEdit, addressEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_doctor);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle("Register");
        init();
    }

    private void init() {
        state = DOCTOR;
        nameEdit = findViewById(R.id.nameEdit);
        ncEdit = findViewById(R.id.ncEdit);
        birthdateEdit = findViewById(R.id.birthdateEdit);
        speciallyEdit = findViewById(R.id.speciallyEdit);
        addressEdit = findViewById(R.id.addressEdit);
        passEdit = findViewById(R.id.passEdit);
        new SetDate(this, birthdateEdit);
        findViewById(R.id.register).setOnClickListener(this::registerClick);
    }

    private void registerClick(View v) {
        EditText e = new EditText(this);
        e.setInputType(InputType.TYPE_CLASS_NUMBER);
        e.setHint("Numeric license.");
        new AlertDialog.Builder(this).setTitle("Verify")
                .setMessage("Please enter license code:")
                .setView(e)
                .setPositiveButton("Apply", (dialog, which) -> {
                    String code = e.getText().toString();
                    AppDatabase.getInstance(this)
                            .licenseDao()
                            .getCodes()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .subscribe(codes -> {
                                if (codes.contains(code)) signUp();
                                else toast("Wrong Code!");
                            }, throwable -> {
                                Log.e("Error#getLicence", throwable.getMessage());
                            });
                })
                .setNegativeButton("Cancel", null)
                .create().show();
    }

    private void signUp() {
        //TODO Validate specially and address
        boolean isValid = Tools.Companion.validateName(nameEdit.getText().toString()) &&
                Tools.Companion.validatePassword(passEdit.getText().toString()) &&
                Tools.Companion.validateNationalCode(ncEdit) && !TextUtils.isEmpty(birthdateEdit.getText().toString()) &&
                !TextUtils.isEmpty(speciallyEdit.getText().toString()) && !TextUtils.isEmpty(addressEdit.getText().toString());
        if (isValid) {
            //Insert doctor
            //TODO validate birth date
            //Tools.Companion.getTimeStamp(birthdateEdit.getText().toString()) > (System.currentTimeMillis() / 1000) - 400000000
            if (false) {
                toast("Not valid birth date.");
            } else
                new Task(() -> {
                    AppDatabase.getInstance(this)
                            .doctorDao()
                            .insert(new Doctor(Tools.Companion.getString(ncEdit),
                                    Tools.Companion.getString(passEdit), Tools.Companion.getString(nameEdit),
                                    Tools.Companion.getString(addressEdit), Tools.Companion.getString(speciallyEdit),
                                    Tools.Companion.getString(birthdateEdit)
                            ));
                }, this::handleResult).execute();
        } else {
            toast("Invalid fields.");
        }
    }

    private void handleResult(boolean r) {
        if (r) {
            Toast.makeText(this, "Welcome sir!", Toast.LENGTH_SHORT).show();
            new UserStore(this)
                    .saveUser(new Doctor(Tools.Companion.getString(ncEdit),
                            Tools.Companion.getString(passEdit), Tools.Companion.getString(nameEdit),
                            Tools.Companion.getString(addressEdit), Tools.Companion.getString(speciallyEdit),
                            Tools.Companion.getString(birthdateEdit)
                    ));
            finishAffinity();
            startActivity(new Intent(this, ir.os.patients.activities.Doctor.class));
        } else toast("Insert to database failed. National code might be taken.");
    }

}
