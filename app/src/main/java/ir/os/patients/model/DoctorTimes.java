package ir.os.patients.model;

public class DoctorTimes {

    private String date, address, phone;
    private int id, hour;
    private boolean taken;

    public DoctorTimes(String date, int id, String address, String phone, int hour, boolean taken) {
        this.date = date;
        this.id = id;
        this.address = address;
        this.phone = phone;
        this.hour = hour;
        this.taken = taken;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
