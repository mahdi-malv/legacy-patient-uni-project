package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {
        @ForeignKey(entity = Admin.class, childColumns = "admin_id", parentColumns = "id", onUpdate = CASCADE),
        @ForeignKey(entity = Building.class, childColumns = "building_id", parentColumns = "id", onUpdate = CASCADE)
},
        indices = {@Index(value = {"admin_id", "building_id"}, unique = true)},
        primaryKeys = {"admin_id", "building_id"})
public class Verify {

    @ColumnInfo(name = "admin_id")
    private int adminId;

    @ColumnInfo(name = "building_id")
    private int buildingId;

    private long date;

    public Verify(int adminId, int buildingId, long date) {
        this.adminId = adminId;
        this.buildingId = buildingId;
        this.date = date;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
