package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import ir.os.patients.model.Appointments;

@Dao
public interface AppointmentDao {

    @Query("select * from appointment where patient_nc = :nc")
    public Flowable<List<Appointment>> getPatientAppointments(String nc);

    @Insert
    public void insert(Appointment appointment);

    @Delete
    public void delete(Appointment appointment);

    //TODO: Delete appointment
//    @Query("delete from appointment where ")
//    public void deleteByValues();

    //join queries

    @Query("select name, date, hour, completed from appointment join doctor on nc = doctor_nc where nc = :nc")
    public Flowable<List<Appointments>> getAppoinmentsForPatient(String nc);

    @Query("update appointment set completed = :completed where date = :date and hour = :hour and patient_nc = :patient_nc and doctor_nc = :doctor_nc")
    public void updateCompleted(String date, int hour, String patient_nc, String doctor_nc, boolean completed);
}
