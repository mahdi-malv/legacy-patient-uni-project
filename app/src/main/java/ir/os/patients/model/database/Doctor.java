package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Doctor(nc,pass,name,specially,birthDate)
 */
@Entity
public class Doctor {

    @PrimaryKey
    @ColumnInfo(name = "nc")
    @NonNull
    private String nationalCode;
    @NonNull
    private String pass;

    @NonNull
    private String name;

    @NonNull
    private String address;

    @NonNull
    private String specially;

    @ColumnInfo(name = "birth_date")
    @NonNull
    private String birthDate;

    public Doctor(@NonNull String nationalCode, @NonNull String pass, @NonNull String name, @NonNull String address, @NonNull String specially, @NonNull String birthDate) {
        this.nationalCode = nationalCode;
        this.pass = pass;
        this.name = name;
        this.address = address;
        this.specially = specially;
        this.birthDate = birthDate;
    }

    @NonNull
    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(@NonNull String nationalCode) {
        this.nationalCode = nationalCode;
    }

    @NonNull
    public String getPass() {
        return pass;
    }

    public void setPass(@NonNull String pass) {
        this.pass = pass;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getSpecially() {
        return specially;
    }

    public void setSpecially(@NonNull String specially) {
        this.specially = specially;
    }

    @NonNull
    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(@NonNull String birthDate) {
        this.birthDate = birthDate;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }
}
