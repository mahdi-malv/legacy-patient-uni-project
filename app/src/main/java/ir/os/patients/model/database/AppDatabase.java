package ir.os.patients.model.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import ir.os.patients.model.Task;

@android.arch.persistence.room.Database(
        entities = {Admin.class, Appointment.class, Building.class, Doctor.class, Patient.class,
                Timing.class, Verify.class, WorkIn.class, License.class, MedicalData.class, History.class},
        version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract AdminDao adminDao();

    public abstract DoctorDao doctorDao();

    public abstract PatientDao patientDao();

    public abstract LicenseDao licenseDao();

    public abstract VerifyDao verifyDao();

    public abstract BuildingDao buildingDao();

    public abstract TimingDao timingDao();

    public abstract HistoryDao historyDao();

    public abstract AppointmentDao appointmentDao();

    public abstract WokInDao wokInDao();

    public static AppDatabase getInstance(Context c) {
        if (instance == null) instance = buildDatabase(c);
        return instance;
    }

    private static AppDatabase buildDatabase(final Context c) {
        return Room.databaseBuilder(c, AppDatabase.class, "app.db")
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        new Task(() -> {
                            AppDatabase.getInstance(c).licenseDao()
                                    .insert(new License("1234567890"),
                                            new License("0987654321"),
                                            new License("1234"));
                            AppDatabase.getInstance(c).adminDao()
                                    .insert(new Admin(
                                            1234, "1234", "David Gilmour",
                                            "1996/08/12", "09381234567"
                                    ));


                        }, result -> {
                        }).execute();
                    }
                }).build();
    }
}
