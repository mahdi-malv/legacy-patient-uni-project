package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface HistoryDao {

    @Insert
    public void insert(History history);

    @Query("select illness from history where patient_nc = :nc")
    public Flowable<List<String>> getPatientIllnesses(String nc);

    @Delete
    public void delete(History history);
}
