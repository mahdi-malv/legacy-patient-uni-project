package ir.os.patients.model;

import android.os.AsyncTask;
import android.util.Log;

/**
 * This class is like a thread
 * But you can pass an doBack and postExecute
 */
public class Task {

    private Fun f;
    private Fun2 f1;

    public Task(Fun f, Fun2 f1) {
        this.f = f;
        this.f1 = f1;
    }

    public void execute() {
        new ToDo(f, f1).execute();
    }

    public interface Fun {
        void get();
    }

    public interface Fun2 {
        void get(boolean result);
    }

    private static class ToDo extends AsyncTask<Void, Void, Boolean> {

        Fun f;
        Fun2 f1;

        ToDo(Fun f, Fun2 f1) {
            this.f = f;
            this.f1 = f1;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                f.get();
                return true;
            } catch (Exception e) {
                Log.e("ERROR#Task", e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean res) {
            f1.get(res);
        }
    }
}
