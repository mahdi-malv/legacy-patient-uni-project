package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface MedicalDataDao {

    @Insert
    void insert(MedicalData data);

    @Query("select * from medical_data")
    Flowable<List<MedicalData>> getAll();

    @Query("select * from medical_data where patient_nc = :nc")
    Flowable<List<MedicalData>> getByNc(String nc);
}
