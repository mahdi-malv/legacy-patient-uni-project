package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Building(id, address, phone)
 */
@Entity
public class Building {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private String address;

    @NonNull
    private String phone;

    @ColumnInfo(name = "doctor_nc")
    @NonNull
    private String doctorNc;

    public Building(@NonNull String address, @NonNull String phone, @NonNull String doctorNc) {
        this.address = address;
        this.phone = phone;
        this.doctorNc = doctorNc;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getDoctorNc() {
        return doctorNc;
    }

    public void setDoctorNc(@NonNull String doctorNc) {
        this.doctorNc = doctorNc;
    }
}
