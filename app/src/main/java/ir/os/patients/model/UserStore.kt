package ir.os.patients.model

import android.content.Context
import android.content.SharedPreferences
import ir.os.patients.model.database.Admin
import ir.os.patients.model.database.Doctor
import ir.os.patients.model.database.Patient


class UserStore(c: Context) {

    //UserStore
    val TYPE = "type"
    val NO_TYPE = -1
    val TYPE_PATIENT = Activity.PATIENT
    val TYPE_DOCTOR = Activity.DOCTOR
    val TYPE_ADMIN = Activity.ADMIN
    val USER_STORE = "user_store"
    val LOG_CHECK = "log_check"

    private val p: SharedPreferences = c.getSharedPreferences(USER_STORE, Context.MODE_PRIVATE)

    val userLogged: Boolean get() = p.getBoolean(LOG_CHECK, false)

    fun getUserType() = p.getInt(TYPE, NO_TYPE)

    private fun setLogged(logged: Boolean) {
        val editor = p.edit()
        editor.putBoolean(LOG_CHECK, logged)
        editor.apply()
    }

    fun saveUser(p: Patient) {
        val editor = this.p.edit()
        editor.clear()
        editor.putString("nc", p.nationalCode)
        editor.putString("phone", p.phone)
        editor.putString("name", p.name)
        editor.putString("pass", p.pass)
        editor.putInt(TYPE, TYPE_PATIENT)
        editor.putBoolean(LOG_CHECK, true)
        editor.apply()
    }

    fun saveUser(d: Doctor) {
        val editor = p.edit()
        editor.clear()
        editor.putString("nc", d.nationalCode)
        editor.putString("name", d.name)
        editor.putString("specially", d.specially)
        editor.putString("address", d.address)
        editor.putString("birth_date", d.birthDate)
        editor.putInt(TYPE, TYPE_DOCTOR)
        editor.putBoolean(LOG_CHECK, true)
        editor.apply()
    }

    fun saveUser(a: Admin) {
        val editor = p.edit()
        editor.clear()
        editor.putInt("id", a.id)
        editor.putString("name", a.name)
        editor.putString("pass", a.pass)
        editor.putString("phone", a.phone)
        editor.putString("birth_date", a.birthDate)
        editor.putInt(TYPE, TYPE_ADMIN)
        editor.putBoolean(LOG_CHECK, true)
        editor.apply()
    }

    fun getPatient() = Patient(
            p.getString("nc", ""),
            p.getString("phone", ""),
            p.getString("name", ""),
            p.getString("pass", "")
    )

    fun getDoctor() = Doctor(
            p.getString("nc", ""),
            p.getString("pass", ""),
            p.getString("name", ""),
            p.getString("address", ""),
            p.getString("specially", ""),
            p.getString("birth_date", "")
    )

    fun getAdmin() = Admin(
            p.getInt("id", 0),
            p.getString("pass", ""),
            p.getString("name", ""),
            p.getString("birth_date", ""),
            p.getString("phone", "")
    )

    fun updatePass(pass: String) {
        p.edit().putString("pass", pass).apply()
    }

    fun updateSpecially(s: String) {
        p.edit().putString("specially", s).apply()
    }

    fun logout() {
        p.edit().clear().apply()
        setLogged(false)
    }
}
