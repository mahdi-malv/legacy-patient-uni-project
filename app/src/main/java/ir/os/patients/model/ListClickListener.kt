package ir.os.patients.model

import android.view.MotionEvent
import android.support.v7.widget.RecyclerView
import android.R.attr.onClick
import android.content.Context
import android.text.method.Touch.onTouchEvent
import android.view.GestureDetector
import android.view.View


class ListClickListener(context: Context, recyclerView: RecyclerView, private val clickListener: ClickListener?)
    : RecyclerView.OnItemTouchListener {

    private val gestureDetector: GestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return true
        }
    })

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {

        val child = rv.findChildViewUnder(e.x, e.y)
        if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            clickListener.onClick(child, rv.getChildLayoutPosition(child))
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

    interface ClickListener {
        fun onClick(view: View, position: Int)
    }
}
