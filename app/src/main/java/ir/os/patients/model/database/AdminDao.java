package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface AdminDao {

    @Insert
    public void insert(Admin... admins);

    @Update
    public void update(Admin admin);

    @Query("select * from admin where id = :id")
    public Single<Admin> getAdmin(int id);

    @Query("update admin set pass = :pass where id = :id")
    public void update(int id, String pass);

    @Query("select * from admin")
    public Flowable<List<Admin>> getAll();
}
