package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "medical_data",
foreignKeys = {@ForeignKey(entity = Patient.class, parentColumns = "nc", childColumns = "patient_nc")})
public class MedicalData {

    @PrimaryKey
    @ColumnInfo(name = "patient_nc")
    @NonNull
    private String patientNc;

    private String illness;

    public MedicalData(@NonNull String patientNc, String illness) {
        this.patientNc = patientNc;
        this.illness = illness;
    }

    @NonNull
    public String getPatientNc() {
        return patientNc;
    }

    public void setPatientNc(@NonNull String patientNc) {
        this.patientNc = patientNc;
    }

    public String getIllness() {
        return illness;
    }

    public void setIllness(String illness) {
        this.illness = illness;
    }
}
