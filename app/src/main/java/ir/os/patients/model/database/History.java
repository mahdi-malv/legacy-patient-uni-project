package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = {@ForeignKey(entity = Patient.class, parentColumns = "nc", childColumns = "patient_nc", onUpdate = CASCADE)},
        primaryKeys = {"patient_nc", "illness"},
        indices = {@Index(value = {"patient_nc", "illness"}, unique = true)}
)
public class History {

    @ColumnInfo(name = "patient_nc")
    @NonNull
    private String patientNc;

    @NonNull
    private String illness;

    public History(@NonNull String patientNc, @NonNull String illness) {
        this.patientNc = patientNc;
        this.illness = illness;
    }

    @NonNull
    public String getPatientNc() {
        return patientNc;
    }

    public void setPatientNc(@NonNull String patientNc) {
        this.patientNc = patientNc;
    }

    @NonNull
    public String getIllness() {
        return illness;
    }

    public void setIllness(@NonNull String illness) {
        this.illness = illness;
    }



}
