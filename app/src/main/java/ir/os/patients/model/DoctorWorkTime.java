package ir.os.patients.model;

public class DoctorWorkTime {
    private String nc, date, address;
    private int hour;
    private boolean taken;

    public DoctorWorkTime(String nc, String date, String address, int hour, boolean taken) {
        this.nc = nc;
        this.date = date;
        this.address = address;
        this.hour = hour;
        this.taken = taken;
    }

    public String getNc() {
        return nc;
    }

    public void setNc(String nc) {
        this.nc = nc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }
}
