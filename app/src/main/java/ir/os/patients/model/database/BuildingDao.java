package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import ir.os.patients.model.DoctorWorkTime;

@Dao
public interface BuildingDao {

    @Query("select * from building")
    public Flowable<List<Building>> getAll();

    @Insert
    public void insert(Building b);

//    @Query("select doctor_nc as nc, date, address, hour, taken from timing inner join building where doctor_nc = :nc and building_id = id")
//    public Flowable<List<DoctorWorkTime>> getDoctorWorkTime(String nc);
}
