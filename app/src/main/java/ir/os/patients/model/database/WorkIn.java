package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "work_in", foreignKeys = {
        @ForeignKey(entity = Building.class, parentColumns = "id", childColumns = "building_id", onUpdate = CASCADE),
        @ForeignKey(entity = Doctor.class, parentColumns = "nc", childColumns = "doctor_nc", onUpdate = CASCADE)
},
        indices = {@Index(value = {"building_id", "doctor_nc"}, unique = true)},
        primaryKeys = {"building_id", "doctor_nc"})
public class WorkIn {

    @ColumnInfo(name = "building_id")
    private int buildingId;

    @ColumnInfo(name = "doctor_nc")
    @NonNull
    private String doctorNc;

    //Nothing more


    public WorkIn(int buildingId, @NonNull String doctorNc) {
        this.buildingId = buildingId;
        this.doctorNc = doctorNc;
    }

    @NonNull
    public String getDoctorNc() {
        return doctorNc;
    }

    public void setDoctorNc(@NonNull String doctorNc) {
        this.doctorNc = doctorNc;
    }

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }
}
