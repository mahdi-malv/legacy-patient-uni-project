package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Admin {

    @PrimaryKey
    private int id;

    @NonNull
    private String pass;

    @NonNull @ColumnInfo(name = "name")
    private String name;

    @NonNull @ColumnInfo(name = "birth_date")
    private String birthDate;

    @NonNull
    private String phone;


    public Admin(int id, @NonNull String pass, @NonNull String name, @NonNull String birthDate, @NonNull String phone) {
        this.id = id;
        this.pass = pass;
        this.name = name;
        this.birthDate = birthDate;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getPass() {
        return pass;
    }

    public void setPass(@NonNull String pass) {
        this.pass = pass;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(@NonNull String birthDate) {
        this.birthDate = birthDate;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
    }
}
