package ir.os.patients.model

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.provider.MediaStore
import android.widget.EditText
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class Tools {

    fun isFirstRun(c: Context, prefName: String): Boolean {
        if (c.getSharedPreferences(prefName, Context.MODE_PRIVATE).getBoolean("isFirstRun", true)) {
            c.getSharedPreferences(prefName, Context.MODE_PRIVATE).edit().putBoolean("isFirstRun", false).apply()
            return true
        }
        return false
    }

    fun getRealPathFromURI(c: Context, contentURI: Uri): String {
        val result: String
        val cursor = c.contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(/*MediaStore.Images.ImageColumns.DATA*/MediaStore.Files.FileColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    companion object {

        fun validatePassword(s: String?) = s != null && !s.isEmpty() && s.length in 4..30

        fun validateNationalCode(e: EditText?): Boolean {
            val s = e?.text?.toString()
            return !s.isNullOrEmpty() && s?.length == 10
        }

        fun validateName(s: String): Boolean {
            val pattern = Pattern.compile("[a-zA-Z ]{7,50}")
            val persianNamePattern = Pattern.compile("[ا-ی ]{7,50}")
            return !s.isEmpty() && !s.startsWith(" ") && !s.contains("  ") &&
                    !s.endsWith(" ") && (pattern.matcher(s).matches() || persianNamePattern.matcher(s).matches())
        }

        fun validateValue(editText: EditText) = validateValue(Integer.parseInt(editText.text.toString()))

        fun validateValue(value: Int) = value >= 0 && value.toString().length < 13

        fun validatePhoneNumber(phone: String) = phone.length == 11 && phone.startsWith("09")

        fun validateWorkPhone(phone: String) = phone.length == 11 &&
                (phone.startsWith("09") || phone.startsWith("0"))

        fun getNumber(e: EditText): Int {
            if (e.text != null && !e.text.toString().isEmpty()) {
                return try {
                    Integer.parseInt(e.text.toString())
                } catch (ex: NumberFormatException) {
                    -1
                }
            }
            return -1
        }

        fun getString(e: EditText?) = if (e == null || e.text == null) "" else e.text.toString()

        fun getString(s: String?) = s ?: ""

        fun getTimeStamp(date: String): Long {
            // date: dd,mm,yyyy
            val df = SimpleDateFormat("dd,MM,yyyy")
            return Timestamp((df.parse(date) as Date).time).time / 1000
        }
    }
}
