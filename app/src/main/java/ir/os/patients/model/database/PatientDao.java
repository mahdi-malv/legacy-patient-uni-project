package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface PatientDao {

    @Insert
    public void insert(Patient patient);

    @Query("select * from patient")
    public Flowable<List<Patient>> getAll();

    @Query("select * from patient where nc = :nc")
    public Single<Patient> getByNc(String nc);
}
