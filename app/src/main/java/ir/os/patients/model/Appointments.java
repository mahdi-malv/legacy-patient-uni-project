package ir.os.patients.model;

public class Appointments {

    private String name;

    private String date;

    private String hour;

    private boolean completed;

    public Appointments(String name, String date, String hour, boolean completed) {
        this.name = name;
        this.date = date;
        this.hour = hour;
        this.completed = completed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}

