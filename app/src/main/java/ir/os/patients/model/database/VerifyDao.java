package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import io.reactivex.Single;

@Dao
public interface VerifyDao {

    @Insert
    void insert(Verify verify);

    @Query("select * from verify where building_id = :id")
    public Single<Verify> getByBuildingId(int id);
}
