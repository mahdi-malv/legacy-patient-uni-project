package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface DoctorDao {

    @Insert
    public void insert(Doctor doctor);

    @Update
    public void update(Doctor doctor);

    @Query("update doctor set specially = :specially where nc = :nc")
    void updateSpecially(String specially, String nc);

    @Query("select * from doctor")
    public Flowable<List<Doctor>> getAll();

    @Query("select * from doctor where nc = :nc")
    public Single<Doctor> getByNc(String nc);

    /**
     * Like must be %skill%
     * @param skill is the speciality
     */
    @Query("select * from doctor where specially like :skill")
    public Flowable<List<Doctor>> getBySpeciality(String skill);


}
