package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Appointment(patientNc, doctorNc, date, hour, completed)
 */
@Entity(
        foreignKeys = {
                @ForeignKey(entity = Patient.class, parentColumns = "nc", childColumns = "patient_nc", onUpdate = CASCADE),
                @ForeignKey(entity = Doctor.class, parentColumns = "nc", childColumns = "doctor_nc", onUpdate = CASCADE)
        },
        indices = {@Index(value = {"patient_nc", "doctor_nc", "date", "hour"}, unique = true)},
        primaryKeys = {"patient_nc", "doctor_nc", "date", "hour"}
)
public class Appointment {

    @ColumnInfo(name = "patient_nc")
    @NonNull
    private String patientNc;

    @ColumnInfo(name = "doctor_nc")
    @NonNull
    private String doctorNc;

    @NonNull
    private String date;

    private int hour;

    private boolean completed;

    public Appointment(@NonNull String patientNc, @NonNull String doctorNc,
                       @NonNull String date, int hour, boolean completed) {
        this.patientNc = patientNc;
        this.doctorNc = doctorNc;
        this.date = date;
        this.hour = hour;
        this.completed = completed;
    }

    @NonNull
    public String getPatientNc() {
        return patientNc;
    }

    public void setPatientNc(@NonNull String patientNc) {
        this.patientNc = patientNc;
    }

    @NonNull
    public String getDoctorNc() {
        return doctorNc;
    }

    public void setDoctorNc(@NonNull String doctorNc) {
        this.doctorNc = doctorNc;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
