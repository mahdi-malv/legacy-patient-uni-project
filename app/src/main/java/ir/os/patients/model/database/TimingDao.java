package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import ir.os.patients.model.DoctorTimes;

@Dao
public interface TimingDao {

    @Insert
    public void insert(Timing timing);

    @Query("select * from timing where doctor_nc = :nc")
    public Flowable<List<Timing>> getByNc(String nc);

    @Query("update timing set taken = :taken where building_id = :id and doctor_nc = :nc and hour = :hour and date = :date")
    public void updateTaken(int id, String nc, boolean taken, String date, int hour);

    @Query("select id, date , address, phone, hour, taken from timing join building on building_id = id and timing.doctor_nc = building.doctor_nc where timing.doctor_nc = :nc")
    public Flowable<List<DoctorTimes>> getDoctorTimes(String nc);
}
