package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

@Dao
public interface WokInDao {

    @Insert
    void insert(WorkIn workIn);
}
