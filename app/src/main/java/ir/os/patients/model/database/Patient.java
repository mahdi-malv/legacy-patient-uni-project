package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Patient Entity (nc, phone, name, pass)
 */
@Entity
public class Patient {

    @PrimaryKey
    @ColumnInfo(name = "nc")
    @NonNull
    private String nationalCode;

    @NonNull
    private String phone;

    @NonNull
    private String name;

    @NonNull
    private String pass;

    public Patient(@NonNull String nationalCode, @NonNull String phone, @NonNull String name, @NonNull String pass) {
        this.nationalCode = nationalCode;
        this.phone = phone;
        this.name = name;
        this.pass = pass;
    }

    @NonNull
    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(@NonNull String nationalCode) {
        this.nationalCode = nationalCode;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getPass() {
        return pass;
    }

    public void setPass(@NonNull String pass) {
        this.pass = pass;
    }
}
