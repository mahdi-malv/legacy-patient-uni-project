package ir.os.patients.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface LicenseDao {

    @Insert
    void insert(License... licenses);

    @Query("select * from license")
    Flowable<List<License>> getAll();

    @Query("select code from license")
    Flowable<List<String>> getCodes();

    @Update
    void update(License license);
}
