package ir.os.patients.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"doctor_nc", "building_id", "date", "hour"},
        foreignKeys = {
                @ForeignKey(entity = Doctor.class, parentColumns = "nc", childColumns = "doctor_nc", onUpdate = CASCADE),
                @ForeignKey(entity = Building.class, parentColumns = "id", childColumns = "building_id", onUpdate = CASCADE)
        },
        indices = {@Index(value = {"doctor_nc", "building_id", "date", "hour"}, unique = true)}
)
public class Timing {

    @ColumnInfo(name = "doctor_nc")
    @NonNull
    private String doctorNc;

    @ColumnInfo(name = "building_id")
    private int buildingId;

    @NonNull
    private String date;

    private int hour;

    private boolean taken;

    public Timing(@NonNull String doctorNc, int buildingId, @NonNull String date,
                  int hour, boolean taken) {
        this.doctorNc = doctorNc;
        this.buildingId = buildingId;
        this.date = date;
        this.hour = hour;
        this.taken = taken;
    }

    @NonNull
    public String getDoctorNc() {
        return doctorNc;
    }

    public void setDoctorNc(@NonNull String doctorNc) {
        this.doctorNc = doctorNc;
    }

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }
}
